# CHANGELOG
[1.0.0](https://gitlab.com/alt-cicd/tools/git/-/tags/1.0.0) - 2023-11-13
===================

### Added
* Initial alt-cicd/tools/git module
