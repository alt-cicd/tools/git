<img src="docs/images/alt-logo.png" alt="alt-cicd" width="300"/>

<a name="heading">alt-cicd module: tools/git</a>
==============================================

<a name="titlebar">Introduction</a>
---------------------------

The **alt-cicd/tools/git**  module provides the base git components required and extended by all other
alt-cicd library modules.

## <a name="contents">Contents</a>
1. [Variables](#variables)
4. [Templates](#templates)
   1. [Variables](#template-variables)
   2. [Rules](#template-rules)
   3. [Scripts](#template-scripts)
   1. [Jobs](#template-jobs)
5. [Jobs](#jobs)
   1. [git](#git)
   1. [git_diff](#git_diff)
   1. [git_status](#git_status)
   1. [git_tag](#git_tag)

<a name="variables">Variables</a>
========================================

All CICD variables are documented in-line.  The current set of release goal variables is:

```yaml
variables:
   CICD_GIT_DIFF_TARGET_BRANCH: $CI_DEFAULT_BRANCH                                         # Target Branch for project release diff
   CICD_GIT_DIFF_SOURCE_BRANCH: $CI_COMMIT_SHA                                             # Source Branch for project release diff
   CICD_GIT_DIFF_GOAL_PATTERN: '/^(pre-release|final-release)$/'                           # Phases the diff runs in
   CICD_GIT_DIFF_FILELIST_PATH: 'diff.files.txt'                                           # Diff file artifact path
   CICD_GIT_DIFF_PATH: 'diff.txt'                                                          # Diff filelist artifact path
   CICD_GIT_USER_NAME: $GITLAB_USER_NAME                                                   # default CICD git user for git operations
   CICD_GIT_USER_EMAIL: $GITLAB_USER_EMAIL                                                 # default CICD git user email for git operations
   CICD_MODULE_TOOLS_GIT: 'true'                                                           # Identifies that the tools-git module is included / active.
   CICD_MODULE_TOOLS_GIT_VERSION: '1.0.0'                                                  # Identifies the tools-git module version
   CICD_RUNTIME_GIT: 'docker:20.0.7-git'                                                   # The default git image used in the library
```

<a name="templates">Templates</a>
======================================

## <a name="template-variables">Variables</a>

The module contains a matching variables template, including the name of the module, which jobs in the module
extend:

```yaml
#----------------------------------------------------------
# Templates - Variables
#----------------------------------------------------------

.tools_git:                                                                               # Module specific variable over-rides, referenced in module jobs
   variables:
      CICD_MODULE_NAME: 'tools-git'
      CICD_MODULE_VERSION: $CICD_MODULE_TOOLS_GIT_VERSION
```
## <a name="template-rules">Rules</a>

None defined.

##<a name="template-scripts">Script</a>

None defined.

## <a name="template-jobs">Jobs</a>

```yaml
.cicd_git:                                                  # convenience template for CICD library git jobs
  extends:
    - .default
    - .tools_git
  image: $CICD_RUNTIME_GIT
  before_script:
    - !reference [.default, script]
    - git config --global user.name "${GITLAB_USER_NAME}"
    - git config --global user.email "${GITLAB_USER_EMAIL}"
    - git remote set-url origin "${CI_SERVER_PROTOCOL}://gitlab-ci-token:${CICD_GITLAB_ACCESS_TOKEN_REPO}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git"

```

<a name="jobs">Jobs</a>
======================================

The tools/git module defines the following jobs:

## <a name="git">git</a>

```yaml
git:
   stage: operate
   extends: .cicd_git
   rules:
      - if: $CICD_JOB == $CI_JOB_NAME
   script:
      - git $ARG1 $ARG2 $ARG3 $ARG4 $ARG5 $ARG6 $ARG7 $ARG8 $ARG9
```
## <a name="git_diff">git_diff</a>

```yaml
      
git_diff:
  extends: .cicd_git
  stage: release
  rules:
    - if: $CICD_JOB == $CI_JOB_NAME
    - !reference [.rules_never_on_cicd_job, rules]
    - if: $CICD_NEVER_ON_MERGE_REQUEST_ENABLED == 'true' && $CI_JOB_NAME =~ $CICD_NEVER_ON_MERGE_REQUEST_PATTERN && $CI_PIPELINE_SOURCE == 'merge_request_event'
      when: never
    - if: $CICD_GIT_DIFF_ENABLED != 'true'
      when: never
    - if: $CICD_GOAL =~ $CICD_GIT_DIFF_GOAL_PATTERN
  script:
    - git fetch origin ${CICD_GIT_DIFF_TARGET_BRANCH}
    - git diff --name-only ${CICD_GIT_DIFF_SOURCE_BRANCH} origin/${CICD_GIT_DIFF_TARGET_BRANCH} > $CICD_GIT_DIFF_FILELIST_PATH
    - git diff  ${CICD_GIT_DIFF_SOURCE_BRANCH} origin/${CICD_GIT_DIFF_TARGET_BRANCH} > $CICD_GIT_DIFF_PATH
    - cat $CICD_GIT_DIFF_PATH
  artifacts:
    paths:
      - $CICD_GIT_DIFF_PATH
      - $CICD_GIT_DIFF_FILELIST_PATH


```
## <a name="git_status">git_status</a>
```yaml
      git_status:
  stage: operate
  extends: .cicd_git
  rules:
    - if: $CICD_JOB == $CI_JOB_NAME
  script:
    - git status


```
## <a name="git_tag">git_tag</a>
```yaml

git_tag:
  stage: release
  extends: .cicd_git
  rules:
    - if: $CICD_JOB == $CI_JOB_NAME
    - !reference [.rules_never_on_cicd_job, rules]
  script:
    - git tag $CICD_TAG -f
    - git push origin $CICD_TAG -f

```


